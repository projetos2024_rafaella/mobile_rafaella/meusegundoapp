import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import Layout from "./layoutdeTelaEstrutura";

export default function LayoutHorizontal() {
  return (
    <View style={styles.container}>
        
      <ScrollView horizontal={true}>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
        <View style={styles.box3}></View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
  },
  box1: {
    width: 50,
    height: 50,
    backgroundColor: "pink",
    borderRadius: 60,
    marginLeft:5,
    marginTop:5,
  },
  box2: {
    width: 50,
    height: 50,
    backgroundColor: "purple",
    borderRadius: 60,
    marginLeft:5,
    marginTop:5,
  },
  box3: {
    width: 50,
    height: 50,
    backgroundColor: "violet",
    borderRadius: 60,
    marginLeft:5,
    marginTop:5,
  },
});
