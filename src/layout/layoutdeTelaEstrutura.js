import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default function Layout() {
  return (
    <View style={styles.container}>
    <View style={styles.header}>
    <Text>MENU</Text>
    </View>

    <View style={styles.content}>
    <ScrollView>
    <Text>CONTEUDO</Text>
    <Text>CONTEUDO</Text>
    <Text>CONTEUDO</Text>
    <Text>CONTEUDO</Text>
    <Text>CONTEUDO</Text>
    <Text>CONTEUDO</Text>
    </ScrollView>
    </View>

    <View style={styles.footer}>
      <Text>RODAPE</Text>
    </View>
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:"column",
    justifyContent: "space-between",
  },
  header:{
    height:50,
    backgroundColor:'blue'
  },
  content:{
    flex:1,
    backgroundColor:'pink'
  },
  footer:{
    height:50,
    backgroundColor:'blue'
  },
});
