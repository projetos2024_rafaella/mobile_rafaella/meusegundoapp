import { StatusBar } from "expo-status-bar";
import {StyleSheet, Text, View, ScrollView, TouchableOpacity,} from "react-native";

export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("TelaInicial")} styele={styles.menu}
      >
        <Text>Opção 1</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
      onPress={() => navigation.navigate("SegundaTela")} styele={styles.menu}
      >
        <Text>Opção 2</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}>
        <Text>Opção 3</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
  },
  menu: {
    padding: 10,
    backgroundColor: "#61E8DF",
    borderRadius: 7,
  },
  text: {
    color: "#FFFAFA",
    fontWeight: "bold",
  },
});
